# Lorentz autodoc example

This repository is an example of introducing automatically generated documentation in a Lorentz contract.

It is dedicated to a particular blogpost that references this repository and so is not assumed to be updated further.

## FAQ

### Where can I see the generated documentation?

You can the documentation for the final version of the contract published in [the adjacent branch](https://gitlab.com/morley-framework/example-lorentz-autodoc/-/blob/autodoc/Counter.md).

Alternatively, clone the repository and do

```sh
stack build
stack exec example-lorentz-autodoc-exec
```

This requires having the [stack tool](https://docs.haskellstack.org/en/stable/README/).

### How can I see changes described by a section X of the article?

You can find the respective commit in [commits history](https://gitlab.com/morley-framework/example-lorentz-autodoc/-/commits/master).

### How do I contribute to this repository?

This repository is finalized and should not be updated further.

In case you want to add examples of contracts with autodoc, you have two options:
1. If your example is simple and uses some fundamental feature, please create an issue to [`morley`](https://gitlab.com/morley-framework/morley) repository and propose adding your example there.
2. For complex interesting examples consider creating your own repository, we will be glad to mention your work in this and `morley` repositories.

### How do I automatically produce documentation in CI?

In the [`morley`](https://gitlab.com/morley-framework/morley) repository we implement two options:
1. Produce the markdown file as artifact. This can later be opened in Markdown editors like [this one](https://jbt.github.io/markdown-editor).
2. Add this file to a dedicated branch of the same repository so that documentation is immediately rendered and the diff can be viewed. In `morley` we do this with the [dedicated script](https://gitlab.com/morley-framework/morley/-/blob/281945c24249fb4224b3e8bc923e3f41428b13bf/scripts/ci/upload-autodoc.sh).
