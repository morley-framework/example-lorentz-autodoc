{-# OPTIONS_GHC -Wno-orphans #-}

module Lorentz.Contracts.Counter
  ( counterContract

    -- * Internals
  , DAuthorization
  ) where

import Lorentz as L

import Fmt (pretty)
import qualified Michelson.Typed.Arith as Arith
import Tezos.Address (unsafeParseAddress)
import Util.Markdown (md, mdSubsection, mdTicked)

----------------------------------------------------------------------------
-- Types
----------------------------------------------------------------------------

newtype Tokens = Tokens Natural
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue, HasAnnotation, Wrappable)

-- Operations permitted over tokens
instance ArithOpHs Arith.Add Tokens Tokens where
  type ArithResHs Arith.Add Tokens Tokens = Tokens
instance ArithOpHs Arith.Sub Tokens Tokens where
  type ArithResHs Arith.Sub Tokens Tokens = Integer

instance TypeHasDoc Tokens where
  typeDocMdDescription = "Tokens amount"

data Parameter
  = Add Tokens
  | Sub Tokens
  | Get (Void_ () Tokens)
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain

type Storage = Tokens

----------------------------------------------------------------------------
-- Doc items
----------------------------------------------------------------------------

data DAuthorization = DAuthorization Address

instance DocItem DAuthorization where
  docItemPos = 10120  -- global position, defines doc items order when rendered
  docItemSectionName = Nothing
  docItemToMarkdown _ (DAuthorization addr) =
    mdSubsection "Authorization" $
      "The sender must be " <> (mdTicked $ pretty addr) <> "."

----------------------------------------------------------------------------
-- Errors
----------------------------------------------------------------------------

type instance ErrorArg "badSubtract" = Integer

instance CustomErrorHasDoc "badSubtract" where
  customErrClass = ErrClassActionException  -- normal user error
  customErrDocMdCause = "Too few tokens for subtraction"
  customErrArgumentSemantics = Just "diff between balance and subtracted amount"

type instance ErrorArg "unauthorized" = ()

instance CustomErrorHasDoc "unauthorized" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Given caller is not authorized to perform update operations"

----------------------------------------------------------------------------
-- Contract
----------------------------------------------------------------------------

-- Hardcoded address for simplicity
adminAddress :: Address
adminAddress = unsafeParseAddress "tz1fXwtPH87YMqH74uWFa5jBVxNKW6MQjpLC"

authorizeUpdate :: s :-> s
authorizeUpdate = do
  doc $ DAuthorization adminAddress
  sender
  push adminAddress
  ifEq nop (failCustom_ #unauthorized)

counterContract :: Contract Parameter Storage
counterContract = defaultContract . docGroup "Counter" $ do
  doc $ DDescription [md|
    A trivial counter contract.

    Used for demonstration purposes.
    |]
  doc $ dStorage @Storage

  unpair
  entryCaseSimple

    ( #cAdd /-> do
        doc $ DDescription "Increase the counter by given value."
        authorizeUpdate

        add
        nil; pair

    , #cSub /-> do
        doc $ DDescription "Decrease the counter by given value."
        authorizeUpdate

        rsub
        dup
        isNat
        if IsSome
          then do dip drop; coerceWrap
          else failCustom #badSubtract
        nil; pair

    , #cGet /-> void_ $ do
        doc $ DDescription "Get the current counter value."
        drop @()

    )
